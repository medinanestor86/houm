import factory
from django.contrib.gis.geos import Point
from django.utils import timezone

from faker import Faker

from apps.checkers.models import ScheduledVisits, ScheduledVisitsLocation
from apps.proprietaries.tests.factories import PropertyFactory
from apps.users.tests.factories import UserFactory

faker = Faker()


class ScheduledVisitsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ScheduledVisits

    date = factory.LazyFunction(timezone.now)
    property = factory.SubFactory(PropertyFactory)


class UserVisitorLocationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ScheduledVisitsLocation

    user = factory.SubFactory(UserFactory)
    schedule_visit = factory.SubFactory(ScheduledVisitsFactory)

    @factory.LazyAttribute
    def location(self):
        return Point(float(faker.longitude()), float(faker.latitude()))
