from django.conf import settings
from django.contrib.gis.db.models import PointField
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from django_fsm import FSMField, transition


class ScheduledVisits(models.Model):
    class VisitTimeRangeChoices(models.TextChoices):
        RANGE_9_TO_13 = "RANGE_9_TO_13", _("De 9 a 13 hs")
        RANGE_13_TO_18 = "RANGE_13_TO_18", _("De 13 a 18 hs")

    class VisitStatusChoices(models.TextChoices):
        NOT_CONFIRMED = "not_confirmed", _("sin confirmar")
        PENDING = "pending", _("Pendiente")
        TRAVELING = "TRAVELING", _("En viaje")
        IN_PROGRESS = "in_progress", _("En curso")
        FINISHED = "finished", _("Finalizada")
        CANCELED_BY_THE_VISITOR = "canceled_by_visitor", _("Cancelada por el visitador")
        CANCELED_BY_OWNER = "canceled_by_owner", _("Cancelada por el propietario")

    user_visitor = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True
    )
    property = models.ForeignKey("proprietaries.Property", on_delete=models.CASCADE)
    date = models.DateField()
    time_range = models.CharField(choices=VisitTimeRangeChoices.choices, max_length=75)
    status = FSMField(
        choices=VisitStatusChoices.choices,
        default=VisitStatusChoices.NOT_CONFIRMED,
        max_length=75,
    )
    start_of_visit = models.DateTimeField(null=True)
    end_of_visit = models.DateTimeField(null=True)
    average_speed = models.PositiveBigIntegerField(null=True)

    @transition(
        field=status,
        source=VisitStatusChoices.NOT_CONFIRMED,
        target=VisitStatusChoices.PENDING,
    )
    def pending(self):
        pass

    @transition(
        field=status,
        source=VisitStatusChoices.PENDING,
        target=VisitStatusChoices.TRAVELING,
    )
    def traveling(self):
        pass

    @transition(
        field=status,
        source=VisitStatusChoices.TRAVELING,
        target=VisitStatusChoices.IN_PROGRESS,
    )
    def in_progress(self):
        self.start_of_visit = timezone.now()

    @transition(
        field=status,
        source=VisitStatusChoices.IN_PROGRESS,
        target=VisitStatusChoices.FINISHED,
    )
    def finished(self):
        self.end_of_visit = timezone.now()

    @transition(
        field=status,
        source=[
            VisitStatusChoices.NOT_CONFIRMED,
            VisitStatusChoices.PENDING,
            VisitStatusChoices.TRAVELING,
        ],
        target=VisitStatusChoices.CANCELED_BY_OWNER,
    )
    def cancel_by_owner(self):
        pass

    @transition(
        field=status,
        source=[
            VisitStatusChoices.NOT_CONFIRMED,
            VisitStatusChoices.PENDING,
            VisitStatusChoices.TRAVELING,
        ],
        target=VisitStatusChoices.CANCELED_BY_THE_VISITOR,
    )
    def cancel_by_visitor(self):
        pass


class ScheduledVisitsLocation(models.Model):
    schedule_visit = models.ForeignKey(ScheduledVisits, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    date = models.DateTimeField(default=timezone.now)
    location = PointField()


class LocationControl(models.Model):
    origin_location = models.ForeignKey(
        ScheduledVisitsLocation, on_delete=models.CASCADE, related_name="origin_points"
    )
    destination_location = models.ForeignKey(
        ScheduledVisitsLocation,
        on_delete=models.CASCADE,
        related_name="destination_points",
    )
    distance_in_mts = models.PositiveBigIntegerField()
    travel_speed = models.PositiveBigIntegerField()
