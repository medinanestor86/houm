from django_filters import rest_framework as filters
from django_fsm import can_proceed
from rest_framework import viewsets, status, mixins
from rest_framework.decorators import action
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response

from apps.checkers.api.v1.filters import ScheduledVisitsFilters, LocationControlFilters
from apps.checkers.api.v1.serializers import (
    ScheduledVisitsSerializer,
    ScheduledVisitsReadOnlySerializer,
    ScheduledVisitsLocationSerializer,
    LocationControlSerializer,
)
from apps.checkers.exceptions import ScheduleVisitTransitionException
from apps.checkers.models import (
    ScheduledVisits,
    ScheduledVisitsLocation,
    LocationControl,
)
from apps.checkers.tasks import collect_data_locations
from utils.rest_framework.permissions import (
    IsReadOnlyUserOrIsSuperUser,
    IsProprietaryUserOrVisitorUser,
    IsVisitorUser,
    IsVisitorUserOrReadOnly,
)


class ScheduledVisitsViewSet(viewsets.ModelViewSet):
    queryset = ScheduledVisits.objects.all()
    permission_classes = (IsReadOnlyUserOrIsSuperUser,)
    serializer_class = ScheduledVisitsSerializer
    filterset_class = ScheduledVisitsFilters
    filter_backends = (filters.DjangoFilterBackend,)

    def get_queryset(self):
        qs = super().get_queryset()
        if self.request.user.is_visitor:
            qs = qs.filter(user_visitor_id=self.request.user.pk)
        elif self.request.user.is_proprietary:
            qs = qs.filter(property__user_proprietary=self.request.user.pk)
        return qs

    @action(
        detail=True,
        methods=["post"],
        url_path="traveling-visits",
        url_name="traveling-visits",
        queryset=ScheduledVisits.objects.all(),
        serializer_class=ScheduledVisitsReadOnlySerializer,
        permission_classes=(IsVisitorUser,),
    )
    def traveling_visit(self, request, pk=None):
        obj_instance = self.get_object()
        if not can_proceed(obj_instance.traveling):
            raise ScheduleVisitTransitionException
        obj_instance.traveling()
        obj_instance.save()
        serializer = self.get_serializer(
            obj_instance, data=self.request.data, partial=True
        )
        serializer.is_valid(raise_exception=False)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(
        detail=True,
        methods=["post"],
        url_path="start-visits",
        url_name="start-visits",
        queryset=ScheduledVisits.objects.all(),
        serializer_class=ScheduledVisitsReadOnlySerializer,
        permission_classes=(IsVisitorUser,),
    )
    def start_visit(self, request, pk=None):
        obj_instance = self.get_object()
        if not can_proceed(obj_instance.in_progress):
            raise ScheduleVisitTransitionException
        obj_instance.in_progress()
        obj_instance.save()
        serializer = self.get_serializer(
            obj_instance, data=self.request.data, partial=True
        )
        serializer.is_valid(raise_exception=False)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(
        detail=True,
        methods=["post"],
        url_path="end-visits",
        url_name="end-visits",
        queryset=ScheduledVisits.objects.all(),
        serializer_class=ScheduledVisitsReadOnlySerializer,
        permission_classes=(IsVisitorUser,),
    )
    def end_visit(self, request, pk=None):
        obj_instance = self.get_object()
        if not can_proceed(obj_instance.finished):
            raise ScheduleVisitTransitionException
        obj_instance.finished()
        obj_instance.save()
        serializer = self.get_serializer(
            obj_instance, data=self.request.data, partial=True
        )
        serializer.is_valid(raise_exception=False)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(
        detail=True,
        methods=["post"],
        url_path="cancel-visits",
        url_name="cancel-visits",
        queryset=ScheduledVisits.objects.all(),
        serializer_class=ScheduledVisitsReadOnlySerializer,
        permission_classes=(IsProprietaryUserOrVisitorUser,),
    )
    def cancel_visit(self, request, pk=None):
        obj_instance = self.get_object()
        if self.request.user.is_visitor:
            if not can_proceed(obj_instance.cancel_by_visitor):
                raise ScheduleVisitTransitionException
            obj_instance.cancel_by_visitor()
        else:
            if not can_proceed(obj_instance.cancel_by_owner):
                raise ScheduleVisitTransitionException
            obj_instance.cancel_by_owner()

        obj_instance.save()
        serializer = self.get_serializer(
            obj_instance, data=self.request.data, partial=True
        )
        serializer.is_valid(raise_exception=False)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ScheduledVisitsLocationViewSet(
    mixins.CreateModelMixin, mixins.ListModelMixin, viewsets.GenericViewSet
):
    queryset = ScheduledVisitsLocation.objects.all()
    permission_classes = (IsVisitorUserOrReadOnly,)
    serializer_class = ScheduledVisitsLocationSerializer

    def get_queryset(self):
        qs = super(ScheduledVisitsLocationViewSet, self).get_queryset()
        filter_kwargs = {"schedule_visit_id": self.kwargs["visits_pk"]}
        if self.request.user.is_visitor:
            filter_kwargs.update(schedule_visit__user_visitor_id=self.request.user.pk)
        elif self.request.user.is_proprietary:
            filter_kwargs.update(
                schedule_visit__property__user_proprietary=self.request.user.pk
            )
        return qs.filter(**filter_kwargs)

    def perform_create(self, serializer):
        instance = serializer.save(
            schedule_visit_id=self.kwargs["visits_pk"],
            user=self.request.user,
        )
        collect_data_locations.delay(location_id=instance.pk)


class VisitorVelocityViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = LocationControl.objects.all()
    serializer_class = LocationControlSerializer
    permission_classes = (IsAdminUser,)
    filterset_class = LocationControlFilters
    filter_backends = (filters.DjangoFilterBackend,)
