from django.urls import path, include
from rest_framework_nested import routers

from .views import (
    ScheduledVisitsViewSet,
    ScheduledVisitsLocationViewSet,
    VisitorVelocityViewSet,
)

router = routers.SimpleRouter()
router.register(r"schedule-visits", ScheduledVisitsViewSet, basename="schedule-visits")
router.register(r"control-speed", VisitorVelocityViewSet, basename="control-speed")

nested_router = routers.NestedSimpleRouter(router, r"schedule-visits", lookup="visits")
nested_router.register(
    r"locations", ScheduledVisitsLocationViewSet, basename="schedule-visits-locations"
)

urlpatterns = [
    path("", include(router.urls)),
    path("", include(nested_router.urls)),
]
