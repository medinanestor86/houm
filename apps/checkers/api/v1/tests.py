from django.test import override_settings
from django.utils import timezone
from rest_framework import status
from rest_framework.test import APITestCase

from apps.checkers.models import ScheduledVisits
from apps.checkers.tests.factories import (
    ScheduledVisitsFactory,
    UserVisitorLocationFactory,
)
from apps.proprietaries.tests.factories import PropertyFactory
from apps.users.tests.factories import (
    UserVisitorFactory,
    UserProprietaryFactory,
    UserAdminFactory,
)
from utils.django.tests.mixins.simple_api import SimpleAPITestCaseMixin


class ScheduledVisitsViewSetNotAuthenticatedUserAPITestCase(
    SimpleAPITestCaseMixin, APITestCase
):
    factory_class = ScheduledVisitsFactory
    base_name_api = "schedule-visits"
    authenticate_user = False

    def test_list(self):
        response = self.case_list()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_detail(self):
        response = self.case_detail()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create(self):
        response = self.case_create()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete(self):
        response = self.case_delete()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update(self):
        response = self.case_update()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_traveling_visit(self):
        entity_case = self.entities[0]
        response = self.case_action(
            "post",
            "traveling-visits",
            url_kwargs={"pk": entity_case.pk},
            request_kwargs={"format": "json", "data": {}},
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_start_visit(self):
        entity_case = self.entities[0]
        response = self.case_action(
            "post",
            "start-visits",
            url_kwargs={"pk": entity_case.pk},
            request_kwargs={"format": "json", "data": {}},
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_end_visit(self):
        entity_case = self.entities[0]
        response = self.case_action(
            "post",
            "end-visits",
            url_kwargs={"pk": entity_case.pk},
            request_kwargs={"format": "json", "data": {}},
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_cancel_visit(self):
        entity_case = self.entities[0]
        response = self.case_action(
            "post",
            "cancel-visits",
            url_kwargs={"pk": entity_case.pk},
            request_kwargs={"format": "json", "data": {}},
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ScheduledVisitsViewSetVisitorUserAPITestCase(SimpleAPITestCaseMixin, APITestCase):
    factory_class = ScheduledVisitsFactory
    base_name_api = "schedule-visits"
    authenticate_user = True
    user_factory_class = UserVisitorFactory

    def setUp(self) -> None:
        super(ScheduledVisitsViewSetVisitorUserAPITestCase, self).setUp()
        self.entities = self.factory_class.create_batch(
            self.quantity_entities_to_create, user_visitor=self.user
        )

    def test_list(self):
        response = self.case_list()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_detail(self):
        response = self.case_detail()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create(self):
        response = self.case_create()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete(self):
        response = self.case_delete()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update(self):
        response = self.case_update()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_traveling_visit(self):
        entity_case = self.entities[0]
        entity_case.status = ScheduledVisits.VisitStatusChoices.PENDING
        entity_case.save()
        response = self.case_action(
            "post",
            "traveling-visits",
            url_kwargs={"pk": entity_case.pk},
            request_kwargs={"format": "json", "data": {}},
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        entity_case.refresh_from_db()
        self.assertEqual(
            entity_case.status, ScheduledVisits.VisitStatusChoices.TRAVELING
        )

    def test_traveling_visit_invalid_state(self):
        entity_case = self.entities[0]
        entity_case.status = ScheduledVisits.VisitStatusChoices.CANCELED_BY_THE_VISITOR
        entity_case.save()
        response = self.case_action(
            "post",
            "traveling-visits",
            url_kwargs={"pk": entity_case.pk},
            request_kwargs={"format": "json", "data": {}},
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_start_visit(self):
        entity_case = self.entities[0]
        entity_case.status = ScheduledVisits.VisitStatusChoices.TRAVELING
        entity_case.save()
        response = self.case_action(
            "post",
            "start-visits",
            url_kwargs={"pk": entity_case.pk},
            request_kwargs={"format": "json", "data": {}},
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        entity_case.refresh_from_db()
        self.assertEqual(
            entity_case.status, ScheduledVisits.VisitStatusChoices.IN_PROGRESS
        )

    def test_start_visit_invalid_status(self):
        entity_case = self.entities[0]
        entity_case.status = ScheduledVisits.VisitStatusChoices.CANCELED_BY_THE_VISITOR
        entity_case.save()
        response = self.case_action(
            "post",
            "start-visits",
            url_kwargs={"pk": entity_case.pk},
            request_kwargs={"format": "json", "data": {}},
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_end_visit(self):
        entity_case = self.entities[0]
        entity_case.status = ScheduledVisits.VisitStatusChoices.IN_PROGRESS
        entity_case.save()
        response = self.case_action(
            "post",
            "end-visits",
            url_kwargs={"pk": entity_case.pk},
            request_kwargs={"format": "json", "data": {}},
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        entity_case.refresh_from_db()
        self.assertEqual(
            entity_case.status, ScheduledVisits.VisitStatusChoices.FINISHED
        )

    def test_end_visit_invalid_status(self):
        entity_case = self.entities[0]
        entity_case.status = ScheduledVisits.VisitStatusChoices.PENDING
        entity_case.save()
        response = self.case_action(
            "post",
            "end-visits",
            url_kwargs={"pk": entity_case.pk},
            request_kwargs={"format": "json", "data": {}},
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_cancel_visit(self):
        entity_case = self.entities[0]
        response = self.case_action(
            "post",
            "cancel-visits",
            url_kwargs={"pk": entity_case.pk},
            request_kwargs={"format": "json", "data": {}},
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        entity_case.refresh_from_db()
        self.assertIn(
            entity_case.status,
            [
                ScheduledVisits.VisitStatusChoices.CANCELED_BY_THE_VISITOR,
                ScheduledVisits.VisitStatusChoices.CANCELED_BY_OWNER,
            ],
        )


class ScheduledVisitsViewSetProprietaryUserAPITestCase(
    SimpleAPITestCaseMixin, APITestCase
):
    factory_class = ScheduledVisitsFactory
    base_name_api = "schedule-visits"
    authenticate_user = True
    user_factory_class = UserProprietaryFactory

    def setUp(self) -> None:
        super(ScheduledVisitsViewSetProprietaryUserAPITestCase, self).setUp()
        self.entities = self.factory_class.create_batch(
            self.quantity_entities_to_create,
            property=PropertyFactory.create(user_proprietary=self.user),
        )

    def test_list(self):
        response = self.case_list()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_detail(self):
        response = self.case_detail()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create(self):
        response = self.case_create()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete(self):
        response = self.case_delete()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update(self):
        response = self.case_update()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_traveling_visit(self):
        entity_case = self.entities[0]
        response = self.case_action(
            "post",
            "traveling-visits",
            url_kwargs={"pk": entity_case.pk},
            request_kwargs={"format": "json", "data": {}},
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_start_visit(self):
        entity_case = self.entities[0]
        response = self.case_action(
            "post",
            "start-visits",
            url_kwargs={"pk": entity_case.pk},
            request_kwargs={"format": "json", "data": {}},
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_end_visit(self):
        entity_case = self.entities[0]
        response = self.case_action(
            "post",
            "end-visits",
            url_kwargs={"pk": entity_case.pk},
            request_kwargs={"format": "json", "data": {}},
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_cancel_visit(self):
        entity_case = self.entities[0]
        response = self.case_action(
            "post",
            "cancel-visits",
            url_kwargs={"pk": entity_case.pk},
            request_kwargs={"format": "json", "data": {}},
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class ScheduledVisitsViewSetAdminUserAPITestCase(SimpleAPITestCaseMixin, APITestCase):
    factory_class = ScheduledVisitsFactory
    base_name_api = "schedule-visits"
    authenticate_user = True
    user_factory_class = UserAdminFactory

    def get_data_to_create_object(self):
        property_instance = PropertyFactory.create()
        return {
            "date": timezone.now().strftime("%Y-%m-%d"),
            "time_range": ScheduledVisits.VisitTimeRangeChoices.RANGE_9_TO_13,
            "status": ScheduledVisits.VisitStatusChoices.NOT_CONFIRMED,
            "property": property_instance.pk,
        }

    def test_list(self):
        response = self.case_list()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            len(response.json()["results"]), self.quantity_entities_to_create
        )

    def test_detail(self):
        response = self.case_detail()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create(self):
        response = self.case_create()
        self.assertEqual(
            response.status_code, status.HTTP_201_CREATED, response.__dict__
        )

    def test_delete(self):
        response = self.case_delete()
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_update(self):
        response = self.case_update()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_traveling_visit(self):
        entity_case = self.entities[0]
        response = self.case_action(
            "post",
            "traveling-visits",
            url_kwargs={"pk": entity_case.pk},
            request_kwargs={"format": "json", "data": {}},
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_start_visit(self):
        entity_case = self.entities[0]
        response = self.case_action(
            "post",
            "start-visits",
            url_kwargs={"pk": entity_case.pk},
            request_kwargs={"format": "json", "data": {}},
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_end_visit(self):
        entity_case = self.entities[0]
        response = self.case_action(
            "post",
            "end-visits",
            url_kwargs={"pk": entity_case.pk},
            request_kwargs={"format": "json", "data": {}},
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_cancel_visit(self):
        entity_case = self.entities[0]
        response = self.case_action(
            "post",
            "cancel-visits",
            url_kwargs={"pk": entity_case.pk},
            request_kwargs={"format": "json", "data": {}},
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ScheduledVisitsLocationViewSetAdminUserAPITestCase(
    SimpleAPITestCaseMixin, APITestCase
):
    factory_class = UserVisitorLocationFactory
    base_name_api = "schedule-visits-locations"
    authenticate_user = True
    user_factory_class = UserAdminFactory

    def setUp(self) -> None:
        super(ScheduledVisitsLocationViewSetAdminUserAPITestCase, self).setUp()
        self.visit_instance = ScheduledVisitsFactory.create()
        self.entities = self.factory_class.create_batch(
            self.quantity_entities_to_create, schedule_visit=self.visit_instance
        )

    def test_list(self):
        response = self.case_list(url_kwargs={"visits_pk": self.visit_instance.pk})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            len(response.json()["results"]), self.quantity_entities_to_create
        )

    def test_create(self):
        response = self.case_create(url_kwargs={"visits_pk": self.visit_instance.pk})
        self.assertEqual(
            response.status_code, status.HTTP_403_FORBIDDEN, response.__dict__
        )


class ScheduledVisitsLocationViewSetProprietaryUserAPITestCase(
    SimpleAPITestCaseMixin, APITestCase
):
    factory_class = UserVisitorLocationFactory
    base_name_api = "schedule-visits-locations"
    authenticate_user = True
    user_factory_class = UserProprietaryFactory

    def test_list(self):
        visit_instance = ScheduledVisitsFactory.create()
        response = self.case_list(url_kwargs={"visits_pk": visit_instance.pk})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create(self):
        visit_instance = ScheduledVisitsFactory.create()
        response = self.case_create(url_kwargs={"visits_pk": visit_instance.pk})
        self.assertEqual(
            response.status_code, status.HTTP_403_FORBIDDEN, response.__dict__
        )


class ScheduledVisitsLocationViewSetVisitorUserAPITestCase(
    SimpleAPITestCaseMixin, APITestCase
):
    factory_class = UserVisitorLocationFactory
    base_name_api = "schedule-visits-locations"
    authenticate_user = True
    user_factory_class = UserVisitorFactory

    def setUp(self) -> None:
        super(ScheduledVisitsLocationViewSetVisitorUserAPITestCase, self).setUp()
        self.visit_instance = ScheduledVisitsFactory.create(user_visitor=self.user)
        time_initial = timezone.now()
        self.entities = []
        for i in range(self.quantity_entities_to_create, 0, -1):
            date = time_initial - timezone.timedelta(minutes=i * 10)
            instance = self.factory_class.create(
                schedule_visit=self.visit_instance, date=date
            )
            self.entities.append(instance)

    def get_data_to_create_object(self):
        return {"location": {"latitude": 5, "longitude": 67}}

    def test_list(self):
        response = self.case_list(url_kwargs={"visits_pk": self.visit_instance.pk})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            len(response.json()["results"]), self.quantity_entities_to_create
        )

    @override_settings(CELERY_TASK_ALWAYS_EAGER=True, CELERY_TASK_EAGER_PROPAGATES=True)
    def test_create(self):
        response = self.case_create(url_kwargs={"visits_pk": self.visit_instance.pk})
        self.assertEqual(
            response.status_code, status.HTTP_201_CREATED, response.__dict__
        )
