from rest_framework import serializers

from utils.rest_framework.fields import LocationField

from ...models import ScheduledVisits, ScheduledVisitsLocation, LocationControl


class ScheduledVisitsSerializer(serializers.ModelSerializer):
    duration_of_visit = serializers.SerializerMethodField(
        read_only=True, method_name="get_duration_of_visit"
    )

    class Meta:
        model = ScheduledVisits
        fields = "__all__"

    @staticmethod
    def get_duration_of_visit(obj):
        if obj.start_of_visit and obj.end_of_visit:
            time_delta = (obj.end_of_visit - obj.start_of_visit).total_seconds() / 60
            return f"{int(time_delta)} min"
        return "-"


class ScheduledVisitsReadOnlySerializer(ScheduledVisitsSerializer):
    class Meta(ScheduledVisitsSerializer.Meta):
        read_only_fields = (
            "user_visitor",
            "property",
            "date",
            "time_range",
            "start_of_visit",
            "end_of_visit",
            "duration_of_visit",
            "status",
        )


class ScheduledVisitsLocationSerializer(serializers.ModelSerializer):
    location = LocationField()

    class Meta:
        model = ScheduledVisitsLocation
        fields = ("location",)
        read_only_fields = ("schedule_visit", "user", "date")


class LocationControlSerializer(serializers.ModelSerializer):
    class Meta:
        model = LocationControl
        fields = "__all__"
        read_only_fields = ("pk",)
