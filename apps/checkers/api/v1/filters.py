from django_filters import rest_framework as filters

from apps.checkers.models import ScheduledVisits, LocationControl


class ScheduledVisitsFilters(filters.FilterSet):
    user = filters.NumberFilter(field_name="user_id")
    date = filters.DateFilter(field_name="date")
    status = filters.ChoiceFilter(choices=ScheduledVisits.VisitStatusChoices.choices)

    class Meta:
        model = ScheduledVisits
        fields = ("user", "status", "date")


class LocationControlFilters(filters.FilterSet):
    user = filters.NumberFilter(field_name="origin_location__user_id")
    date = filters.DateFilter(field_name="origin_location__date")
    speed = filters.NumberFilter(field_name="travel_speed", lookup_expr="gte")

    class Meta:
        model = LocationControl
        fields = (
            "user",
            "date",
            "speed",
        )
