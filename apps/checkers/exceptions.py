from rest_framework.exceptions import APIException


class ScheduleVisitTransitionException(APIException):
    status_code = 400
    default_detail = "Status change not allowed."
    default_code = "status_invalid"
