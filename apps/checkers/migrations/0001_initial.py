# Generated by Django 4.0.1 on 2022-01-07 22:44

from django.conf import settings
import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion
import django_fsm


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ("proprietaries", "0001_initial"),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name="ScheduledVisits",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("date", models.DateField()),
                (
                    "time_range",
                    models.CharField(
                        choices=[
                            ("RANGE_9_TO_13", "De 9 a 13 hs"),
                            ("RANGE_13_TO_18", "De 13 a 18 hs"),
                        ],
                        max_length=75,
                    ),
                ),
                (
                    "status",
                    django_fsm.FSMField(
                        choices=[
                            ("not_confirmed", "sin confirmar"),
                            ("pending", "Pendiente"),
                            ("TRAVELING", "En viaje"),
                            ("in_progress", "En curso"),
                            ("finished", "Finalizada"),
                            ("canceled_by_visitor", "Cancelada por el visitador"),
                            ("canceled_by_owner", "Cancelada por el propietario"),
                        ],
                        default="not_confirmed",
                        max_length=75,
                    ),
                ),
                ("start_of_visit", models.DateTimeField(null=True)),
                ("end_of_visit", models.DateTimeField(null=True)),
                ("average_speed", models.PositiveBigIntegerField(null=True)),
                (
                    "property",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="proprietaries.property",
                    ),
                ),
                (
                    "user_visitor",
                    models.ForeignKey(
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="ScheduledVisitsLocation",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("date", models.DateTimeField(auto_now=True)),
                ("location", django.contrib.gis.db.models.fields.PointField(srid=4326)),
                (
                    "schedule_visit",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="checkers.scheduledvisits",
                    ),
                ),
                (
                    "user",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="LocationControl",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("distance_in_mts", models.PositiveBigIntegerField()),
                ("travel_speed", models.PositiveBigIntegerField()),
                (
                    "destination_location",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="destination_points",
                        to="checkers.scheduledvisitslocation",
                    ),
                ),
                (
                    "origin_location",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="origin_points",
                        to="checkers.scheduledvisitslocation",
                    ),
                ),
            ],
        ),
    ]
