from django.apps import AppConfig


class CheckersConfig(AppConfig):
    name = "apps.checkers"
