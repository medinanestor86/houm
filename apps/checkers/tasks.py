import logging

from apps.checkers.models import LocationControl, ScheduledVisitsLocation
from project.celery import app


logger = logging.getLogger(__name__)


@app.task
def collect_data_locations(location_id):
    try:
        from django.contrib.gis.measure import Distance

        location_instance = ScheduledVisitsLocation.objects.get(pk=location_id)
        last_location = (
            ScheduledVisitsLocation.objects.filter(
                schedule_visit=location_instance.schedule_visit
            )
            .order_by("date")
            .exclude(pk=location_id)
            .last()
        )
        if last_location:
            distance = Distance(
                m=last_location.location.distance(location_instance.location)
            )
            delta = (location_instance.date - last_location.date).total_seconds() / 3600
            speed = distance.km / delta
            LocationControl.objects.create(
                origin_location=last_location,
                destination_location=location_instance,
                distance_in_mts=distance.m,
                travel_speed=speed,
            )

    except ScheduledVisitsLocation.DoesNotExist:
        logger.exception("point location not found")
