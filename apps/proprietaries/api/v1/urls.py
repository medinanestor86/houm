from django.urls import path, include
from rest_framework import routers

from .views import CountryViewSet, CityViewSet, PropertyViewSet

router = routers.SimpleRouter()
router.register(r"countries", CountryViewSet, basename="countries")
router.register(r"cities", CityViewSet, basename="cities")
router.register(r"properties", PropertyViewSet, basename="properties")

urlpatterns = [
    path("", include(router.urls)),
]
