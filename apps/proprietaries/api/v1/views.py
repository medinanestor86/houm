from rest_framework import viewsets, permissions

from apps.proprietaries.api.v1.serializers import (
    CountrySerializer,
    CitySerializer,
    PropertySerializer,
)
from apps.proprietaries.models import Country, City, Property
from utils.rest_framework.permissions import (
    ReadOnlyOrUserAdmin,
    IsProprietaryUserOrIsSuperUser,
)


class CountryViewSet(viewsets.ModelViewSet):
    """
    create: Only admin users can create and modify. The rest have read-only access
    """

    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    permission_classes = (ReadOnlyOrUserAdmin,)
    lookup_field = "pk"


class CityViewSet(viewsets.ModelViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer
    permission_classes = (ReadOnlyOrUserAdmin,)
    lookup_field = "pk"


class PropertyViewSet(viewsets.ModelViewSet):
    queryset = Property.objects.all()
    serializer_class = PropertySerializer
    permission_classes = (permissions.IsAuthenticated, IsProprietaryUserOrIsSuperUser)

    def get_queryset(self):
        qs = super().get_queryset()
        if self.request.user.is_proprietary:
            return qs.filter(user_proprietary_id=self.request.user.pk)
        return qs

    def perform_create(self, serializer):
        serializer.save(user_proprietary_id=self.request.user.pk)
