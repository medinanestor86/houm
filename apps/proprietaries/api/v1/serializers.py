from rest_framework import serializers

from apps.proprietaries.models import Country, City, Property
from utils.rest_framework.fields import LocationField


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = (
            "pk",
            "name",
        )


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = (
            "pk",
            "country",
            "name",
        )
        read_only_fields = ("pk",)


class PropertySerializer(serializers.ModelSerializer):
    location = LocationField()

    class Meta:
        model = Property
        fields = "__all__"
        read_only_fields = ("pk", "user_proprietary")
