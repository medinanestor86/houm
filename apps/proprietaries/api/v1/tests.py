from rest_framework import status
from rest_framework.test import APITestCase

from apps.proprietaries.tests.factories import (
    CityFactory,
    CountryFactory,
    PropertyFactory,
)
from apps.users.tests.factories import (
    UserVisitorFactory,
    UserProprietaryFactory,
    UserAdminFactory,
)
from utils.django.tests.mixins.simple_api import SimpleAPITestCaseMixin


class CityViewSetNotAuthenticatedUserReadOnlyAPITestCase(
    SimpleAPITestCaseMixin, APITestCase
):
    factory_class = CityFactory
    base_name_api = "cities"
    authenticate_user = False

    def test_list(self):
        response = self.case_list()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_detail(self):
        response = self.case_detail()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create(self):
        response = self.case_create()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete(self):
        response = self.case_delete()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update(self):
        response = self.case_update()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class CityViewSetVisitorUserReadOnlyAPITestCase(SimpleAPITestCaseMixin, APITestCase):
    factory_class = CityFactory
    base_name_api = "cities"
    authenticate_user = True
    user_factory_class = UserVisitorFactory

    def test_list(self):
        response = self.case_list()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_detail(self):
        response = self.case_detail()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create(self):
        response = self.case_create()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete(self):
        response = self.case_delete()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update(self):
        response = self.case_update()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class CityViewSetProprietaryUserReadOnlyAPITestCase(
    SimpleAPITestCaseMixin, APITestCase
):
    factory_class = CityFactory
    base_name_api = "cities"
    authenticate_user = True
    user_factory_class = UserProprietaryFactory

    def test_list(self):
        response = self.case_list()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_detail(self):
        response = self.case_detail()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create(self):
        response = self.case_create()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete(self):
        response = self.case_delete()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update(self):
        response = self.case_update()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class CityViewSetAdminUserReadOnlyAPITestCase(SimpleAPITestCaseMixin, APITestCase):
    factory_class = CityFactory
    base_name_api = "cities"
    authenticate_user = True
    user_factory_class = UserAdminFactory

    def get_data_to_create_object(self):
        country = CountryFactory.create()
        return {"country": country.pk, "name": "cordoba"}

    def get_data_to_update_object(self):
        return {"name": "Cordoba"}

    def test_list(self):
        response = self.case_list()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_detail(self):
        response = self.case_detail()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create(self):
        response = self.case_create()
        self.assertEqual(
            response.status_code, status.HTTP_201_CREATED, response.__dict__
        )

    def test_delete(self):
        response = self.case_delete()
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_update(self):
        entity_case = self.entities[0]
        response = self.case_update(entity_case)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEquals(entity_case.name, "Cordoba")
        entity_case.refresh_from_db()
        self.assertEqual(entity_case.name, "Cordoba")


class CountryViewSetNotAuthenticatedUserReadOnlyAPITestCase(
    SimpleAPITestCaseMixin, APITestCase
):
    factory_class = CountryFactory
    base_name_api = "countries"
    authenticate_user = False

    def test_list(self):
        response = self.case_list()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_detail(self):
        response = self.case_detail()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create(self):
        response = self.case_create()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete(self):
        response = self.case_delete()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update(self):
        response = self.case_update()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class CountryViewSetProprietaryUserReadOnlyAPITestCase(
    SimpleAPITestCaseMixin, APITestCase
):
    factory_class = CountryFactory
    base_name_api = "countries"
    authenticate_user = True
    user_factory_class = UserProprietaryFactory

    def test_list(self):
        response = self.case_list()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_detail(self):
        response = self.case_detail()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create(self):
        response = self.case_create()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete(self):
        response = self.case_delete()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update(self):
        response = self.case_update()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class CountryViewSetVisitorUserReadOnlyAPITestCase(SimpleAPITestCaseMixin, APITestCase):
    factory_class = CountryFactory
    base_name_api = "countries"
    authenticate_user = True
    user_factory_class = UserVisitorFactory

    def test_list(self):
        response = self.case_list()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_detail(self):
        response = self.case_detail()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create(self):
        response = self.case_create()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete(self):
        response = self.case_delete()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update(self):
        response = self.case_update()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class CountryViewSetAdminUserReadOnlyAPITestCase(SimpleAPITestCaseMixin, APITestCase):
    factory_class = CountryFactory
    base_name_api = "countries"
    authenticate_user = True
    user_factory_class = UserAdminFactory

    def get_data_to_create_object(self):
        return {"name": "argentina"}

    def get_data_to_update_object(self):
        return {"name": "Argentina"}

    def test_list(self):
        response = self.case_list()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_detail(self):
        response = self.case_detail()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create(self):
        response = self.case_create()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_delete(self):
        response = self.case_delete()
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_update(self):
        entity_case = self.entities[0]
        response = self.case_update(entity_case)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEquals(entity_case.name, "Argentina")
        entity_case.refresh_from_db()
        self.assertEqual(entity_case.name, "Argentina")


class PropertiesViewSetNotAuthenticatedUserReadOnlyAPITestCase(
    SimpleAPITestCaseMixin, APITestCase
):
    factory_class = PropertyFactory
    base_name_api = "properties"
    authenticate_user = False

    def test_list(self):
        response = self.case_list()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_detail(self):
        response = self.case_detail()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create(self):
        response = self.case_create()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete(self):
        response = self.case_delete()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update(self):
        response = self.case_update()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class PropertiesViewSetVisitorUserAPITestCase(SimpleAPITestCaseMixin, APITestCase):
    factory_class = PropertyFactory
    base_name_api = "properties"
    authenticate_user = True
    user_factory_class = UserVisitorFactory

    def test_list(self):
        response = self.case_list()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_detail(self):
        response = self.case_detail()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create(self):
        response = self.case_create()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete(self):
        response = self.case_delete()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update(self):
        response = self.case_update()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class PropertiesViewSetProprietaryUserAPITestCase(SimpleAPITestCaseMixin, APITestCase):
    factory_class = PropertyFactory
    base_name_api = "properties"
    authenticate_user = True
    user_factory_class = UserProprietaryFactory

    def setUp(self) -> None:
        super(PropertiesViewSetProprietaryUserAPITestCase, self).setUp()
        self.entities = self.factory_class.create_batch(
            self.quantity_entities_to_create, user_proprietary=self.user
        )
        self.other_entities = self.factory_class.create_batch(
            self.quantity_entities_to_create,
        )

    def get_data_to_create_object(self):
        city_instance = CityFactory.create()
        return {
            "location": {"latitude": 5, "longitude": 56},
            "title": "Hermoso dpto. luminoso",
            "description": "Dpto en la mejor zona",
            "price": "5000.0000",
            "city": city_instance.pk,
        }

    def test_list(self):
        response = self.case_list()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            len(response.json()["results"]), self.quantity_entities_to_create
        )

    def test_detail(self):
        entity_case = self.entities[0]
        response = self.case_detail(entity_case)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_detail_other_owner(self):
        entity_case = self.other_entities[0]
        response = self.case_detail(entity_case)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create(self):
        response = self.case_create()
        self.assertEqual(
            response.status_code, status.HTTP_201_CREATED, response.__dict__
        )

    def test_delete(self):
        entity_case = self.entities[0]
        response = self.case_delete(entity_case)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_other_owner(self):
        entity_case = self.other_entities[0]
        response = self.case_delete(entity_case)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_update(self):
        entity_case = self.entities[0]
        response = self.case_update(entity_case)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_other_owner(self):
        entity_case = self.other_entities[0]
        response = self.case_update(entity_case)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class PropertiesViewSetAdminUserAPITestCase(SimpleAPITestCaseMixin, APITestCase):
    factory_class = PropertyFactory
    base_name_api = "properties"
    authenticate_user = True
    user_factory_class = UserAdminFactory

    def setUp(self) -> None:
        super(PropertiesViewSetAdminUserAPITestCase, self).setUp()
        self.entities = self.factory_class.create_batch(
            self.quantity_entities_to_create, user_proprietary=self.user
        )
        self.other_entities = self.factory_class.create_batch(
            self.quantity_entities_to_create,
        )

    def get_data_to_create_object(self):
        city_instance = CityFactory.create()
        return {
            "location": {"latitude": 5, "longitude": 56},
            "title": "Hermoso dpto. luminoso",
            "description": "Dpto en la mejor zona",
            "price": "5000.0000",
            "city": city_instance.pk,
        }

    def test_list(self):
        response = self.case_list()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            len(response.json()["results"]), self.quantity_entities_to_create * 2
        )

    def test_detail(self):
        entity_case = self.entities[0]
        response = self.case_detail(entity_case)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_detail_other_owner(self):
        entity_case = self.other_entities[0]
        response = self.case_detail(entity_case)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create(self):
        response = self.case_create()
        self.assertEqual(
            response.status_code, status.HTTP_201_CREATED, response.__dict__
        )

    def test_delete(self):
        entity_case = self.entities[0]
        response = self.case_delete(entity_case)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_other_owner(self):
        entity_case = self.other_entities[0]
        response = self.case_delete(entity_case)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_update(self):
        entity_case = self.entities[0]
        response = self.case_update(entity_case)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_other_owner(self):
        entity_case = self.other_entities[0]
        response = self.case_update(entity_case)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
