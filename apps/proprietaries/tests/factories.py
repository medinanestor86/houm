import factory

from django.contrib.gis.geos import Point

from faker import Faker

from ..models import Property, Country, City

faker = Faker()


class CountryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Country

    name = factory.Sequence(lambda n: f"country name {n}")


class CityFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = City

    country = factory.SubFactory(CountryFactory)
    name = factory.Sequence(lambda n: f"city name {n}")


class PropertyFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Property

    price = factory.LazyAttribute(
        lambda a: faker.pydecimal(left_digits=5, right_digits=4, positive=True)
    )
    city = factory.SubFactory(CityFactory)

    @factory.LazyAttribute
    def location(self):
        return Point(float(faker.longitude()), float(faker.latitude()))
