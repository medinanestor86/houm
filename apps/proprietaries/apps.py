from django.apps import AppConfig


class proprietariesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "apps.proprietaries"
