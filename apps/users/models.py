from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    is_visitor = models.BooleanField(default=False)
    is_proprietary = models.BooleanField(default=False)
