#Backend Tech Lead Challenge HOUM

# Tabla de Contenido

1. [El challenge](#el-challenge)
   1. [Problema](#problema)
   2. [Requisitos](#requisitos)
   3. [Observaciones](#observaciones) 
2. [Analisis](#analisis)
   1. [Python y Django](#python-y-django)
   2. [Problemática y definiciones](#problematica-y-definiciones)   
      1. [requerimientos minimos](#requerimientos-minimos)
      1. [Entidades identificadas](#entidades-identificadas)
      2. [Identificar usuarios](#identificacion-de-usuarios)
      3. [Responsabilidades mínimas de los usuarios](#responsabilidades-minimas-de-los-usuarios)
3. [El proyecto](#el-proyecto)
   1. [Correr el proyecto](#correr-en-proyecto)
   2. [Tipos de usuarios](#tipos-de-usuarios)  
   2. [Documentacion APIs](#documentacion-apis)  
   3. [Test](#tests) 
   4. [Coverage](#coverage) 
   5. [Gitflow](#gitflow)   
   6. [Herramientas utiles](#herramientas-utiles)
   7. [Pendientes](#pendientes)

# El Challenge

### Problema

En Houm tenemos un gran equipo de Houmers que muestran las propiedades y 
solucionan todos los problemas que podrían ocurrir en ellas. 
Ellos son parte fundamental de nuestra operación y de la experiencia que tienen nuestros clientes. 
Es por esta razón que queremos incorporar ciertas métricas para monitorear cómo operan, 
mejorar la calidad de servicio y para asegurar la seguridad de nuestros Houmers.


### Requisitos

Crear un servicio REST que:
1. Permita que la aplicación móvil mande las coordenadas del Houmer
2. Para un día retorne todas las coordenadas de las propiedades que visitó y cuanto tiempo se quedó en cada una.
3. Para un día retorne todos los momentos en que el houmer se trasladó con una velocidad superior a cierto parámetro


### Observaciones
El lenguaje de programación es a elección, dentro de los siguientes lenguajes: Python, Typescript, Javascript.

La solución debe ser de calidad de producción.

Incluir instrucciones de cómo ejecutar el código localmente
Cualquier supuesto que se haya realizado tiene que ser documentado y justificado.
Tienes 5 días para entregar tu respuesta
Envíanos tus respuestas en una repo pública al correo techtalent@houm.com con copia a daniela.bustos@houm.com


# Analisis
Con el objetivo de tener un idea  clara de como encarar el challenge  decidí
realizar un pequeño análisis en el cual surgieron los siguientes puntos.

### Python y Django.
Se definio el uso de Python como lenguaje de programacion a utilizar y basaremos
la construccion de la solucion sobre Django.

info: https://www.djangoproject.com/

### Problematica y definiciones.

#### requerimientos minimos.

> Permita que la aplicación móvil mande las coordenadas del Houmer.

Se debe permitir el manejo de datos geo-espaciales para esto nos apoyaremos en las herramientas
que nos provee django junto con PostGIS.

GeoDjango info: https://docs.djangoproject.com/en/4.0/ref/contrib/gis/

PostGIS: https://postgis.net/

> 2. Para un día retorne todas las coordenadas de las propiedades que visitó y cuanto tiempo se quedó en cada una.

Basándonos en esta premisa se infiere que una de las entidades que participa del flujo
son las propiedades con su respectiva localización.

Para simplificar el seguimiento de las propiedades visitadas, se implementara una entidad para
representar las mismas. Estás pasaran por diferentes estados que representara el flujo de la 
visita programada

> 3. Para un día retorne todos los momentos en que el houmer se trasladó con una velocidad superior a cierto parámetro

TODO: por resolver.

#### Entidades identificadas.
Basándonos en las premisas anteriores se identificaron las siguientes entidades participantes.

![entidades!](./docs/img/entidades_participantes.png "entidades")


#### Flujo simplificado

![flujo_visitas!](./docs/img/flujo_visitas.png "flujo_visitas")

#### Identificacion de usuarios.
Se identificaron tres perfiles de usuarios dentro del flujo planteado por el challenge.

![user types!](./docs/img/users_types.png "users_types")

#### Responsabilidades minimas de los usuarios.

**Administradores**:
   1. Administrar countries y cities.
   2. Crear, listar, cambiar status a las visitas programadas.
   3. Crear, listar, editar propiedades.
   4. Ver localización de los visitadores.


**Visitadores**:
   1. Listar countries y cities.
   2. Listar, cambiar status según flujo a las visitas programadas que les fueron asignadas.


**Propietarios**:
   1. Listar countries y cities.
   2. Listar, cambiar status según flujo a las visitas programadas a sus propiedades.
   3. Crear, listar, editar propiedades.


#### Posibles estados de las visitas programadas.

![visits status!](./docs/img/visits_status.png "visits_status")


# El proyecto.

## Correr en proyecto

Instalar docker https://docs.docker.com/engine/install/

```commandline
docker-compose build api
docker-compose up api
```

## Tipos de usuarios.

### Admin
Usuario Administrador puede gestionar las propiedades y realizar consultas sobre los diferentes usuarios.


```commandline
user: admin
password: admin
```


### Visitor
El usuario visitador puede ver las propiedades a visitar y registrar su ubicación.


```commandline
user: visitor
password: visitor
```


### Proprietary
El usuario propietario puede administrar sus propiedades y en forma reducida las visitas programadas.

```commandline
user: proprietary
password: proprietary
```

## Documentacion APIs.
Para documentar las APIS se uso el generador de swagger [drf-yasg](https://drf-yasg.readthedocs.io/en/stable/) 
puede acceder a la misma ingresando a http://0.0.0.0:8000/swagger/

## Tests
Para correr los tests ejecute el siguiente comando.
```commandline
docker-compose run api python manage.py test -v 3
```

## Coverage
Para visualizer el coverage del proyecto puede ejecutar los siguientes comandos.

```commandline
docker-compose run api coverage  run --source='.' manage.py test
docker-compose run api coverage report
docker-compose run api coverage html
```


## Herramientas utiles

### Flake8

Flake8 es una librería de Python que contiene PyFlakes, pycodestyle y el script
McCabe de Ned Batchelder. Es un gran conjunto de herramientas para verificar su 
código fuente contra el PEP8, errores de programación (como “library imported but unused” y “Undefined name”) 
y para verificar la complejidad ciclomática.

```commandline
flake8 .
```

info: https://flake8.pycqa.org/en/latest/

### Black

Black no es un linter, no nos sugiere que podemos corregir. Black corrige lo que está mal en nuestro código, 
apoyándose en los estándares.

```commandline
black .
```

info: https://black.readthedocs.io/en/stable/index.html

### pre-commit

Pre-Commit un framework Open Source. Su propósito es gestionar y 
mantener Git Hooks de múltiples lenguajes. Pretende ser 
un estándar de configuración para el hook pre-commit. Es una 
librería Python capaz de manejar hooks de CSS, JS, Node.js, 
Go, Python, etc.

```commandline
# instalar pre-commit
$ pip install pre-commit
# verificar la instalacion
$ pre-commit --version
pre-commit 2.16.0
# instalar hooks
$ pre-commit install
pre-commit installed at .git/hooks/pre-commit
```

correr todos los hooks

```commandline
$ pre-commit run --all-files
```

info: https://pre-commit.com/


## Gitflow
Para el desarrollo del tests se utilizo el siguiente esquema de gitflow

### Master branch
Cualquier commit que pongamos en esta rama debe estar preparado para pasar a producción.
Cada vez que se agrega código a master tenemos una nueva versión del producto.

### Staging or Develop branch
Branch que contiene el código que conformará la próxima versión 
planificada del proyecto. (Rama de integración)

### Realease branches
Branch que contiene el código que conformará las diferentes versiones del sistema.
Agrupan ramas de features y bugfix, se mantienen actualizados con respecto a master (para releases que aún no están en producción)
y el 'release candidate' se debe fusionar tanto en master como en staging. 

### Hotfix branches
Estas branchs corrigen errores y bugs en el código de producción.
Funcionan de manera similar a las branch releases con la diferencia que las correciones no fueron
planeadas. Salen y se fusionan en master.

### Feature/Bugfix branches
Branchs donde se ubica el código que corresponde al desarrollo de un ticket.
Salen de la branch release en donde fue planeado  y se fusionan contra él.

![gitflow!](./docs/img/gitflow.png "gitflow")

## Pendientes
1. No se realizo un analisis del modelado de propiedades el mismo es a modo de ejemplo.
2. No tengo experiencia trabajando con datos geo espaciales, seguro hay una forma mejor de hacer las cosas.
3. Agregar mas filtros
4. Optimizacion de base de datos indices, queries, etc.
5. Mover logica de DB a managers de django.
6. configurar gitlab pipelines para CI.
7. Agregar zona de cobertura para los visitadores para una mejor asignacion de visitas.
8. Agregar agenda para visitadores.
9. Asignacion automatica de visitas por zona de cobertura.
10. datos iniciales.





