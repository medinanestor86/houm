from rest_framework import permissions


class ReadOnlyOrUserStaff(permissions.BasePermission):
    def has_permission(self, request, view):
        return bool(
            request.method in permissions.SAFE_METHODS
            or request.user
            and request.user.is_authenticated
            and request.user.is_staff
        )


class ReadOnlyOrUserAdmin(permissions.BasePermission):
    def has_permission(self, request, view):
        return bool(
            (
                request.method in permissions.SAFE_METHODS
                and request.user.is_authenticated
            )
            or request.user
            and request.user.is_authenticated
            and request.user.is_superuser
        )


class IsProprietaryUserOrIsSuperUser(permissions.BasePermission):
    def has_permission(self, request, view):
        return bool(
            request.user and (request.user.is_superuser or request.user.is_proprietary)
        )


class IsProprietaryUserOrVisitorUser(permissions.BasePermission):
    def has_permission(self, request, view):
        return bool(
            request.user
            and request.user.is_authenticated
            and (request.user.is_visitor or request.user.is_proprietary)
        )


class IsReadOnlyUserOrIsSuperUser(permissions.BasePermission):
    def has_permission(self, request, view):
        return bool(
            (
                request.method in permissions.SAFE_METHODS
                and request.user.is_authenticated
                and (request.user.is_proprietary or request.user.is_visitor)
            )
            or request.user
            and request.user.is_authenticated
            and request.user.is_superuser
        )


class IsSuperUser(permissions.BasePermission):
    def has_permission(self, request, view):
        return bool(request.user and request.user.is_superuser)


class IsVisitorUser(permissions.BasePermission):
    def has_permission(self, request, view):
        return bool(
            request.user and request.user.is_authenticated and request.user.is_visitor
        )


class IsVisitorUserOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        return bool(
            (
                request.method in permissions.SAFE_METHODS
                and request.user.is_authenticated
                and (request.user.is_proprietary or request.user.is_superuser)
            )
            or request.user
            and request.user.is_authenticated
            and request.user.is_visitor
        )
