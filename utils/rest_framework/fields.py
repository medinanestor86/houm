from django.conf import settings
from drf_extra_fields.geo_fields import PointField


class LocationField(PointField):
    """
    Django rest framework field for point representation.
    """

    class Meta:
        if settings.ENABLED_SWAGGER_DOC:
            from drf_yasg import openapi

            swagger_schema_fields = {
                "type": openapi.TYPE_OBJECT,
                "title": "Location",
                "properties": {
                    "latitude": openapi.Schema(
                        title="point latitude",
                        type=openapi.TYPE_NUMBER,
                    ),
                    "longitude": openapi.Schema(
                        title="Point longitude",
                        type=openapi.TYPE_NUMBER,
                    ),
                },
                "required": ["latitude", "longitude"],
            }
