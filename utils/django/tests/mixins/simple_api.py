from django.shortcuts import reverse

from apps.users.tests.factories import UserFactory


class ActionEnum:
    LIST = "list"
    DETAIL = "detail"
    CREATE = "list"
    DELETE = "detail"
    UPDATE = "detail"


class UserCreatorMixin:
    """
    this class adds the possibility to create a user to be authenticated in the tests
    """

    user_factory_class = UserFactory

    def create_user(self):
        """
        Create and return a user
        """
        return self.user_factory_class.create()


class BaseSimpleAPITestCaseMixin:
    """
    create and run generic test cases in apis
    """

    factory_class = None
    quantity_entities_to_create = 10
    base_name_api = None
    lookup_url_field = "pk"
    lookup_entities_detail_field = "pk"

    def setUp(self) -> None:
        # create models instances from factories
        self.entities = self.factory_class.create_batch(
            self.quantity_entities_to_create
        )

    def get_data_to_create_object(self):
        """
        Get data for post method case
        """
        return {}

    def get_data_to_update_object(self):
        """
        Get data for update method case
        """
        return {}

    def case_list(self, url_kwargs=None):
        url = self.get_url(ActionEnum.LIST, url_kwargs=url_kwargs)
        response = self.client.get(url)
        return response

    def case_detail(self, instance=None, url_kwargs=None):
        url = self.get_url(ActionEnum.DETAIL, instance=instance, url_kwargs=url_kwargs)
        response = self.client.get(url)
        return response

    def case_create(self, url_kwargs=None):
        # url = reverse(f"{self.base_name_api}-list")
        url = self.get_url(ActionEnum.CREATE, url_kwargs=url_kwargs)
        data = self.get_data_to_create_object()
        response = self.client.post(url, data=data, format="json")
        return response

    def case_update(self, instance=None, url_kwargs=None):
        url = self.get_url(ActionEnum.UPDATE, instance=instance, url_kwargs=url_kwargs)
        data = self.get_data_to_update_object()
        response = self.client.patch(url, data)
        return response

    def case_delete(self, instance=None, url_kwargs=None):
        url = self.get_url(ActionEnum.DELETE, instance=instance, url_kwargs=url_kwargs)
        response = self.client.delete(url)
        return response

    def case_action(self, method, action, url_kwargs, request_kwargs):
        url = reverse(f"{self.base_name_api}-{action}", kwargs=url_kwargs)
        response = getattr(self.client, method)(url, **request_kwargs)
        return response

    def get_url(self, action, instance=None, url_kwargs=None):
        if not instance:
            instance = self.entities[0]
        url_name = f"{self.base_name_api}-{action}"
        kwargs = url_kwargs if url_kwargs else {}
        if action == "detail":
            kwargs.update(
                {
                    self.lookup_url_field: getattr(
                        instance, self.lookup_entities_detail_field
                    )
                }
            )
        url = reverse(
            url_name,
            kwargs=kwargs,
        )
        return url


class SimpleAPITestCaseMixin(UserCreatorMixin, BaseSimpleAPITestCaseMixin):
    authenticate_user = False

    def setUp(self) -> None:
        super(SimpleAPITestCaseMixin, self).setUp()
        self.user = self.create_user()
        if self.authenticate_user:
            self.client.force_authenticate(user=self.user)
